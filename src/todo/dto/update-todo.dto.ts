import { PartialType } from '@nestjs/mapped-types';
import { IsBoolean, IsOptional } from 'class-validator';
import { CreateTodoDto } from './create-todo.dto';

export class UpdateTodoDto extends PartialType(CreateTodoDto) {

  @IsBoolean()
  @IsOptional()
  done?: boolean;

  // ? Esto es de typescrip diciendo que es opcional

}
